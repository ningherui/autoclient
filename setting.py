#作为一个配置文件
# PLUGIN_CLASS_LIST = [
#     'lib.plugins.disk.DiskPlugin',
#     'lib.plugins.memory.MemoryPlugin',
#     'lib.plugins.network.NetworkPlugin',
# ]
import os
PLUGIN_CLASS_DICT = {
    "disk": 'lib.plugins.disk.DiskPlugin',
    "memory": 'lib.plugins.memory.MemoryPlugin',
    "network": 'lib.plugins.network.NetworkPlugin',
    "board":   'lib.plugins.board.BoardPlugin',
    "basic":   'lib.plugins.basic.BasicPlugin',
    "cpu":   'lib.plugins.cpu.CpuPlugin',
}


SSH_USER='root'
SSH_PORT='22'

#定义一个文件的位置,使用setting的上一级作为根目录
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
#日志路径和文件路径重新定义
# LOGGING_PATH = 'log/cmdb.log'
LOGGING_PATH = os.path.join(BASE_DIR,'log/cmdb.log')
LOCAL_DISK_FILE_PATH = os.path.join(BASE_DIR,'file/disk.out')