"""
用于采集内存信息
"""
from .base import BasePlugin
from lib.utils.log import logger
from lib.utils.response import BaseResponse
from  lib.utils import convert
import traceback
class MemoryPlugin(BasePlugin):
    """
    采集硬盘信息
    """
    # def process(self,ssh,hostname):
    #     result = ssh(hostname, "free -h |grep Mem|awk '{print $2}'")
    #     return result

    def process(self,ssh,hostname):
        response = BaseResponse()
        try:
            result = ssh(hostname,'dmidecode -q -t 17 2>/dev/null')
            #对数据进行处理
            response.data = self.parse(result)
            # print(response.data)
        except Exception as e:
            logger.error(traceback.format_exc())
            response.status = False
            response.error = traceback.format_exc()
        return response.dict
    def parse(self,content):
        '''
        解析shell命令返回的结果
        :param content:shell 命令结果
        :return:解析后的结果
        '''
        ram_dict = {}
        key_map = {
            'Size': 'capacity',
            'Locator': 'slot',
            'Type': 'model',
            'Speed': 'speed',
            'Manufacturer': 'manufacturer',
            'Serial Number': 'sn',
        }
        device = content.split('Memory Device')
        for item in device:
            item = item.strip()
            if not item:
                continue
            if item.startswith('#'):
                continue
            segment = {}
            lines = item.split('\n\t')
            for line in lines:
                if len(line.split(':')) > 1:
                    key , vaule = line.split(':')
                else:
                    key = line.split(':')[0]
                    vaule = ""
                if key in key_map:
                    if key == "Size":
                        segment[key_map['Size']] = convert.convert_mb_to_gb(vaule,0)
                    else:
                        segment[key_map[key.strip()]] = vaule.strip()
            ram_dict[segment['slot']] = segment
        return ram_dict
