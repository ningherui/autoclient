from .base import BasePlugin
from lib.utils.log import logger
from lib.utils.response import BaseResponse
from  lib.utils import convert
import traceback
import os
import re
class NetworkPlugin(BasePlugin):
    """
    用于采集网卡信息
    """
    # def process(self,ssh,hostname):
    #     result = ssh(hostname, 'ifconfig ens33|grep -v inet6|grep inet|awk \'{print $2}\'')
    #     return result
    def process(self,ssh,hostname):
        response = BaseResponse()
        try:
            cmd1= ssh(hostname,'ip link show')
            cmd2= ssh(hostname,'ip addr show')
            ifaces = self._interfaces_ip(cmd1 + '\n' + cmd2)
            # print(ifaces)
            #对数据进行处理
            # response.data = self.parse(result)
        except Exception as e:
            logger.error(traceback.format_exc())
            response.status = False
            response.error = traceback.format_exc()
        return response.dict

    def which(self,exe):
        def _is_executable_file_or_link(exe):
            #check for os.X_OK doesn't suffice because directory may exectable
            return (os.access(exe,os.X_OK) and
                (os.path.isfile(exe) or os.path.islink(exe)))
        if exe:
            if _is_executable_file_or_link(exe):
                # executable in cwd or fullpath
                return  exe

        default_path = '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin'
        search_path = os.environ.get('PATH',default_path)
        path_ext = os.environ.get('PATHEXT','.EXE')
        ext_list = path_ext.split(';')
        search_path = search_path.split(os.pathsep)
        if True:
            search_path.extend(
                [
                    x for x in default_path.split(os.pathsep)
                    if x not in search_path
                ]
            )
        for path in search_path:
            full_path = os.path.join(path,exe)
            if _is_executable_file_or_link(full_path):
                return full_path
        return None

    def _number_of_set_bits_to_ipv4_netmask(self,set_bits): #pylint: disable=C0103

        '''
        Return an IPV4 netmask from the integer representation of that mask
        Ex. Oxffffff00 > '255.255.255.0'
        :param set_bites:
        :return:
        '''

        return self.cidr_to_ipv4_netmask(self._number_of_set_bits(set_bits))

    def cidr_to_ipv4_netmask(self,cidr_bits):
        '''
        return an IPV4 netmask
        :param cidr_bits:
        :return:
        '''
        try:
            cidr_bits = int(cidr_bits)
            if not 1 <= cidr_bits <=32:
                return ''
        except ValueError:
            return ''
        netmask = ''
        for idx in range(4):
            if idx:
                netmask += '.'
            if cidr_bits >= 8:
                netmask += '255'
                cidr_bits -= 8
            else:
                netmask += '{0:d}'.format(256 - (2**(8 - cidr_bits)))
                cidr_bits = 0
        return netmask

    def _number_of_set_bits(self, x):
        '''
        return the number of bits that are set in a 32bit int
        :param x:
        :return:
        '''
        x -= (x >> 1) & Ox55555555
        x = ((x >> 2) & Ox33333333 ) +(x & Ox33333333)
        x = ((x >> 4) + x) & OxOfOfOfOf
        x += x >> 8
        x += x >> 16
        return  x &  OxOOOOOO3f


    def _interfaces_ip(self, out):
        '''
        Uses ip to return a dictionary of interfaces with various information about each (up/down state,ip address,netmask,and hwaddr)
        :param out:
        :return:
        '''
        ret = dict()
        right_keys = ['name', 'hwaddr','up', 'netmask', 'ipaddrs']

        def parse_network(value,cols):
            brd = None
            if '/' in value:
                ip, cidr = value.split('/')
            else:
                ip = value
                cidr = 32
            if type_ == 'inet':
                mask = self.cidr_to_ipv4_netmask(int(cidr))
                if 'brd' in cols:
                    brd = cols[cols.index('brd') +1]
            return (ip, mask, brd)
        groups = re.compile('\r?\n\\d').split(out)
        for group in groups:
            iface = None
            data = dict()
            for line in group.splitlines():
                if ' ' not  in line:
                    continue
                match = re.match(r'^\d*:\s+([\w.\-]+)(?:@)?([\w.\-]+)?:\s+<(.+)>', line)
                if match:
                    iface,parent,attrs = match.groups()
                    if 'UP' in attrs.split(','):
                        data['up'] = True
                    else:
                        data['up'] = False
                    if parent and parent in right_keys:
                        data[parent] = parent
                    continue
                cols = line.split()
                if len(cols) >=2:
                    type_, value = tuple(cols[0:2])
                    iflabel = cols[-1:][0]
                    if type_ in ('inet',):
                        if 'secondary' not in cols:
                            ipaddr, netmask, broadcast = parse_network(value, cols)
                            if type_ == 'inet':
                                if 'inet' not in data:
                                    data['inet'] = list()
                                addr_obj = dict()
                                addr_obj['address'] = ipaddr
                                addr_obj['netmask'] = netmask
                                addr_obj['broadcast'] = broadcast
                                data['inet'].append(addr_obj)
                        else:
                            if 'secondary' not in data:
                                data['secondary'] = list()
                            ip_, mask, brd = parse_network(value,cols)
                            data['secondary'].append({
                                'type': type_,
                                'address': ip_,
                                'netmask': mask,
                                'boardcast': brd,
                            })
                            del ip_,mask,brd
                    elif type_.startswith('link'):
                        data['hwaddr'] = value
            if iface:
                if iface.startswith('pan') or iface.startswith('lo') or iface.startswith('v'):
                    del iface, data
                else:
                    ret[iface] = data
                    del iface,data
        return ret
    def standard(self,interfaces_info):
        for key,value in interfaces_info.items():
            ipaddrs = set()
            netmask = set()
            if not 'iner' in value:
                value['ipaddrs'] = ''
                value['netmask'] = ''
            else:
                for item in value['imet']:
                    ipaddrs.add(item['address'])
                    netmask.add(item['netmask'])
                value['ipaddrs'] = '/'.join(ipaddrs)
                value['netmask'] = '/'.join(netmask)
                del value['inet']