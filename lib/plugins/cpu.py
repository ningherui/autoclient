from .base import BasePlugin
from lib.utils.log import logger
from lib.utils.response import BaseResponse
import traceback
class CpuPlugin(BasePlugin):
    """
    采集系统基本信息,主机名,版本
    """


    def process(self,ssh,hostname):
        response = BaseResponse()
        try:
            cpu_info = ssh(hostname,"cat /proc/cpuinfo")
            response.data = self.parse(cpu_info)

            #print(result.data)
        except Exception as e:
            logger.error(traceback.format_exc())
            response.status = False
            response.error = traceback.format_exc()
        return response.dict
    @staticmethod
    def parse(content):
        response = {
            'cpu_count': 0,
            'cpu_physical_count': 0,
            'cpu_model': ''
        }
        cpu_physical_set = set()
        content = content.strip()
        for item in content.split('\n\n'):
            for row_line in item.split('\n'):
                key, value = row_line.split(':')
                key = key.strip()
                if key == 'processor':
                    response['cpu_count'] +=1
                elif key == 'physical id':
                    cpu_physical_set.add(value)
                elif key == 'model name':
                    if not response['cpu_model']:
                        response['cpu_model'] = value
        response['cpu_physical_count'] = len(cpu_physical_set)
        return response