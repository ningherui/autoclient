class BasePlugin(object):
    """
    基类,用于做约束.约束子类中必须实现process方法
    """
    def process(self,ssh,hostname):
        raise NotImplementedError("%s中必须实现Process方法" %self.__class__.__name__)