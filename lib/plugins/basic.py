from .base import BasePlugin
from lib.utils.log import logger
from lib.utils.response import BaseResponse
import traceback
class BasicPlugin(BasePlugin):
    """
    采集系统基本信息,主机名,版本
    """


    def process(self,ssh,hostname):
        response = BaseResponse()
        try:
            uname = ssh(hostname,"uname -r").strip()
            host_name = ssh(hostname,"hostname").strip()
            version = ssh(hostname,"cat /etc/redhat-release").strip().split()[3]
            response.data = {
                'uname': uname,
                'host_name': host_name,
                'Version': version
            }

            #print(result.data)
        except Exception as e:
            logger.error(traceback.format_exc())
            response.status = False
            response.error = traceback.format_exc()
        return response.dict