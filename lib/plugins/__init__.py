import importlib
import setting

def get_server_info(ssh,hostname):
    server_info = {}
    for key,path in setting.PLUGIN_CLASS_DICT.items():
        #key = "disk", path = "lib.plugins.disk.DiskPlugin"
        """
            使用下面的语句从右向左根据地一个点(.)分成两个字符串,
            lib.plugins.disk  DiskPlugin
        """
        module_path,class_name = path.rsplit('.',maxsplit=1)
        #根据一个字符串导入一个模块,需要使用importlib的模块
        module = importlib.import_module(module_path)   #等价于import lib.plugins.disk
        #获取一个类的对象
        cls = getattr(module,class_name)
        #print(key,cls())
        plugin_object = cls()
        info = plugin_object.process(ssh,hostname)
        server_info[key] = info
    return server_info