def convert_to_int(vaule,default=0):
    try:
        result = int(vaule)
    except Exception as e:
        result = default
    return result

def convert_mb_to_gb(vaule,default=0):
    try:
        vaule = vaule.strip('MB')
        result = int(vaule)
    except Exception as e:
        result = default
    return  result