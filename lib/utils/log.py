import logging
import setting
class Logger():
    def __init__(self,log_file_path,level):
        file_haddler = logging.FileHandler(log_file_path, 'a', encoding='utf-8')
        fmt = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s - %(module)s: %(message)s')
        file_haddler.setFormatter(fmt)

        self.logger = logging.Logger('cmdb',level=level)
        self.logger.addHandler(file_haddler)

    def error(self,msg):
        self.logger.error(msg)

#调用这个logger模块,写入日志
logger = Logger(setting.LOGGING_PATH,logging.DEBUG)

