# 创建类库/模块目录lib
# from lib.plugins.disk    import DiskPlugin
# from lib.plugins.memory  import MemoryPlugin
# from lib.plugins.network import NetworkPlugin
# 
# def run():
#     #实例化对象
#     obj1 = DiskPlugin()
#     disk_info = obj1.process()
# 
#     obj2 = MemoryPlugin()
#     memory_info = obj2.process()
#     
#     obj3 = NetworkPlugin()
#     Network_info = obj3.process()
#     
# if __name__ == '__main__':
#     run()
import importlib
import requests
import setting
from lib.plugins import get_server_info
#引入线程池
from concurrent.futures import ThreadPoolExecutor
def ssh(hostname,cmd):
    import paramiko
    private_key = paramiko.RSAKey.from_private_key_file('/home/ningherui/.ssh/id_rsa')
    # 创建SSH对象
    ssh = paramiko.SSHClient()
    # 允许连接不在know_hosts文件中的主机
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # 连接服务器
    ssh.connect(hostname=hostname, port=setting.SSH_PORT, username=setting.SSH_USER, pkey=private_key)
    # 执行命令
    stdin, stdout, stderr = ssh.exec_command(cmd)
    # 获取命令结果
    result = stdout.read()
    # 关闭连接
    ssh.close()
    return result.decode('utf-8')
# def run():
#    for key,path in setting.PLUGIN_CLASS_DICT.items():
#         #key = "disk", path = "lib.plugins.disk.DiskPlugin"
#         """
#             使用下面的语句从右向左根据地一个点(.)分成两个字符串,
#             lib.plugins.disk  DiskPlugin
#         """
#         module_path,class_name = path.rsplit('.',maxsplit=1)
# #根据一个字符串导入一个模块,需要使用importlib的模块
#         module = importlib.import_module(module_path)   #等价于import lib.plugins.disk
#         #获取一个类的对象
#         cls = getattr(module,class_name)
#         #print(key,cls())
#         plugin_object = cls()
#         info = plugin_object.process()
#         print(key,info)
def task(hostname):
    server_info = get_server_info(ssh, hostname)
    print(server_info['board'])
    result = requests.post(
        url='http://127.0.0.1:8000/api/get_data',
        json={'host': hostname, 'info': server_info}
    )
    print('把资产信息发送到API', result.text)
def run():
    #获取__init__.py的get_server_info方法的到的数据,获取两个参数
    # hostname = '192.168.100.101'
    pool = ThreadPoolExecutor(10)
    host_list = [
        '192.168.100.101',
        '192.168.100.102',
        '192.168.100.103',
    ]
    for hostname in host_list:
        pool.submit(task,hostname)
        # print(server_info)
    # server_info = get_server_info(ssh,hostname)
    # print(server_info)

if __name__ == '__main__':
    run()
